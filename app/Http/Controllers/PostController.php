<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct(Request $request)
    {

        $this->setConstruct($request, PostResource::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::paginate($this->perPage, ['*'], 'page', $this->page);

        return $this->collection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePostRequest $request
     * @return void
     */
    public function store(StorePostRequest $request)
    {
        $post = new Post($request->only(['title', 'body']));
        $post->user_id = Auth::id();
        $post->save();

        return $this->resource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $post = Post::find($id);

        if (!$post)
            return $this->error(404, 'NOT FOUND!');

        return $this->resource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePostRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $post = Post::find($id);

        if (!$post)
            return $this->error(404, 'NOT FOUND!');

        $post->update($request->only(['title', 'body']));

        return $this->resource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if (!$post || $post->user_id != Auth::id())
            return $this->error(404, 'NOT FOUND!');

        $post->delete();

        return $this->success([], 'Deleted Successfully!');
    }

    public function getMyPosts()
    {
        $posts = Post::where('user_id', Auth::id())->paginate($this->perPage, ['*'], 'page', $this->page);

        return $this->collection($posts);
    }
}
