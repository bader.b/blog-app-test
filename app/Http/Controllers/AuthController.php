<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponser;

    public function login(LoginRequest $request)
    {

        if (!Auth::attempt(['email' => $request['email'], 'password' => $request['password']]))
            return $this->error(401, 'Credentials not match');

        $user = auth()->user();
        return $this->success([
            'user' => new UserResource($user),
            'token' => auth()
                ->user()
                ->createToken('Login API Token')
                ->plainTextToken
        ], 'You Have Logged in Successfully');
    }

    public function register(RegisterRequest $request)
    {

        $user = User::create([
            'name' => $request['name'],
            'password' => bcrypt($request['password']),
            'email' => $request['email'],
        ]);

        return $this->success([
            'token' => $user->createToken('Register API Token')->plainTextToken
        ], 'You Have Registered Successfully');
    }

    public function logout()
    {

        auth()->user()->tokens()->delete();
        return $this->success([], 'You Have Successfully Logged out');
    }
}
